В данном репозитории находится дефолтный конфигурационный файл nginx.conf, который был скачан с официального репозитория https://github.com/nginx/nginx/blob/master/conf/nginx.conf.

Partners nginx:
1. Amazon Web Services
2. Google Cloud Platform
3. IBM
4. Microsoft Azure
5. Red Hat

HTTP proxy and Web server features of nginx:
* Ability to handle more than 10,000 simultaneous connections with a low memory footprint (~2.5 MB per 10k inactive HTTP keep-alive connections)
* Handling of static files, index files and auto-indexing
* Reverse proxy with caching
* Load balancing with in-band health checks
* TLS/SSL with SNI and OCSP stapling support, via OpenSSL
* FastCGI, SCGI, uWSGI support with caching
* gRPC support since March 2018, version 1.13.10
* Name- and IP address-based virtual servers
* IPv6-compatible
* WebSockets since 1.3.13, including acting as a reverse proxy and do load balancing of WebSocket applications
* HTTP/1.1 Upgrade (101 Switching Protocols), HTTP/2 protocol support
* URL rewriting and redirection